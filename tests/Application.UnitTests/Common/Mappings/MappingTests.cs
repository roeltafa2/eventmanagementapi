﻿using AutoMapper;
using EventManagementAPI.Application.EventItems.Queries.GetEvent;
using EventManagementAPI.Application.EventItems.Queries.GetEventList;
using EventManagementAPI.Domain.Entities;
using System;
using Xunit;

namespace EventManagementAPI.Application.UnitTests.Common.Mappings
{
    public class MappingTests : IClassFixture<MappingTestsFixture>
    {
        private readonly IConfigurationProvider _configuration;
        private readonly IMapper _mapper;

        public MappingTests(MappingTestsFixture fixture)
        {
            _configuration = fixture.ConfigurationProvider;
            _mapper = fixture.Mapper;
        }

        [Fact]
        public void ShouldHaveValidConfiguration()
        {
            _configuration.AssertConfigurationIsValid();
        }
        
        [Theory]
        [InlineData(typeof(Event), typeof(EventDTO))]
        [InlineData(typeof(Event), typeof(EventViewModel))]
        public void ShouldSupportMappingFromSourceToDestination(Type source, Type destination)
        {
            var instance = Activator.CreateInstance(source);

            _mapper.Map(instance, source, destination);
        }
    }
}
