﻿using EventManagementAPI.Domain.Interfaces;
using System;

namespace EventManagementAPI.WebUI.IntegrationTests
{
    public class TestDateTimeService : IDateTime
    {
        public DateTime Now => DateTime.Now;
    }
}
