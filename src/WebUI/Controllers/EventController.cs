﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EventManagementAPI.Application.EventItems.Commands.CreateEvent;
using EventManagementAPI.Application.EventItems.Commands.DeleteEvent;
using EventManagementAPI.Application.EventItems.Commands.UpdateEvent;
using EventManagementAPI.Application.EventItems.Queries.GetEvent;
using EventManagementAPI.Application.EventItems.Queries.GetEventList;
using EventManagementAPI.WebUI.Controllers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebUI.Controllers
{
    public class EventController : ApiController
    {
        // GET: api/Event
        [HttpGet]
        public async Task<ActionResult<EventListViewModel>> Get()
        {
            var vm = await Mediator.Send(new GetEventListQuery());

            return base.Ok(vm);
        }

        // GET: api/Event/5
        [HttpGet("{id}")]
        public async Task<ActionResult<EventViewModel>> Get(int id)
        {
            var vm = await Mediator.Send(new GetEventQuery { Id = id });

            return base.Ok(vm);
        }

        // POST: api/Event
        [HttpPost]
        public async Task<ActionResult<long>> Post([FromBody] CreateEventCommand command)
        {
            var response = await Mediator.Send(command);

            return Ok(response);
        }

        // PUT: api/Event/5
        [HttpPut()]
        public async Task<IActionResult> Put([FromBody] UpdateEventCommand command)
        {
            await Mediator.Send(command);

            return NoContent();
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            await Mediator.Send(new DeleteEventCommand { Id = id });

            return NoContent();
        }
    }
}
