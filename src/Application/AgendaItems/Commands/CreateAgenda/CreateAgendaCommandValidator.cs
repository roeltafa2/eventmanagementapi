﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace EventManagementAPI.Application.AgendaItems.Commands.CreateAgenda
{
    public class CreateAgendaCommandValidator : AbstractValidator<CreateAgendaCommand>
    {
        public CreateAgendaCommandValidator()
        {
            RuleFor(v => v.Description)
             .NotEmpty();
        }
    }
}
