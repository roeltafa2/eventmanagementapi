﻿using EventManagementAPI.Application.Common.Exceptions;
using EventManagementAPI.Application.Common.Interfaces;
using EventManagementAPI.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EventManagementAPI.Application.AgendaItems.Commands.CreateAgenda
{
    public class CreateAgendaCommand : IRequest<long>
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime Time { get; set; }
        public string TimeFrom { get; set; }
        public string TimeTo { get; set; }
        public long EventId { get; set; }
    }

    public class CreateAgendaCommandhandler : IRequestHandler<CreateAgendaCommand, long>
    {
        private readonly IApplicationDbContext _context;
        public CreateAgendaCommandhandler(IApplicationDbContext context)
        {
            _context = context;
        }
        public async Task<long> Handle(CreateAgendaCommand request, CancellationToken cancellationToken)
        {
            var eventEntity = _context.Event.FindAsync(request.EventId);
            if (eventEntity == null)
            {
                throw new NotFoundException(nameof(Agenda), request);
            }

            var entity = new Agenda();

            entity.EventId = request.EventId;
            entity.Time = request.Time;
            entity.TimeFrom = request.TimeFrom;
            entity.TimeTo = request.TimeTo;
            entity.Title = request.Title;
            entity.Description = request.Description;

            await _context.Agenda.AddAsync(entity);
            await _context.SaveChangesAsync(cancellationToken);

            return entity.Id;

        }
    }
}
