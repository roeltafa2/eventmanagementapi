﻿using EventManagementAPI.Application.Common.Exceptions;
using EventManagementAPI.Application.Common.Interfaces;
using EventManagementAPI.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EventManagementAPI.Application.EventItems.Commands.UpdateEvent
{
    public class UpdateEventCommand : IRequest
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public DateTime StartDate { get; set; }
        public bool OneDayEvent { get; set; }
        public DateTime EndDate { get; set; }
        public string Location { get; set; }
        public string Description { get; set; }
    }

    public class UpdateEventCommandHandler : IRequestHandler<UpdateEventCommand>
    {
        private readonly IApplicationDbContext _context;

        public UpdateEventCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }
        public async Task<Unit> Handle(UpdateEventCommand request, CancellationToken cancellationToken)
        {
            var entity = await _context.Event.FindAsync(request.Id);

            if (entity == null)
            {
                throw new NotFoundException(nameof(Event), request.Id);
            }

            entity.Name = request.Name;
            entity.StartDate = request.StartDate;
            entity.OneDayEvent = request.OneDayEvent;
            entity.EndDate = request.EndDate;
            entity.Location = request.Location;
            entity.Description = request.Description;

            await _context.SaveChangesAsync(cancellationToken);

            return Unit.Value;

        }
    }
}
