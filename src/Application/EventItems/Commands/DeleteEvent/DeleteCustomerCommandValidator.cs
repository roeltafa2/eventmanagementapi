﻿using FluentValidation;

namespace EventManagementAPI.Application.EventItems.Commands.DeleteEvent
{
    public class DeleteCustomerCommandValidator : AbstractValidator<DeleteEventCommand>
    {
        public DeleteCustomerCommandValidator()
        {
            RuleFor(v => v.Id).NotEmpty();
        }
    }
}
