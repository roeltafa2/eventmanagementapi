﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace EventManagementAPI.Application.EventItems.Commands.DeleteEvent
{
    public class DeleteEventCommand : IRequest
    {
        public long Id { get; set; }
    }
}
