﻿using EventManagementAPI.Application.Common.Interfaces;
using EventManagementAPI.Domain.Entities;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace EventManagementAPI.Application.EventItems.Commands.CreateEvent
{
    public class CreateEventCommand : IRequest<long>
    {
        public string Name { get; set; }
        public DateTime StartDate { get; set; }
        public bool OneDayEvent { get; set; }
        public DateTime EndDate { get; set; }
        public string Location { get; set; }
        public string Description { get; set; }        
    }

    public class CreateEventCommandHanlder : IRequestHandler<CreateEventCommand, long>
    {
        private readonly IApplicationDbContext _context;
        public CreateEventCommandHanlder(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<long> Handle(CreateEventCommand request, CancellationToken cancellationToken)
        {
            var entity = new Event
            {
                Name = request.Name,
                StartDate = request.StartDate,
                OneDayEvent = request.OneDayEvent,
                EndDate = request.EndDate,
                Location = request.Location,
                Description = request.Description
            };

            _context.Event.Add(entity);

            await _context.SaveChangesAsync(cancellationToken);

            return entity.Id;
        }
    }
}
