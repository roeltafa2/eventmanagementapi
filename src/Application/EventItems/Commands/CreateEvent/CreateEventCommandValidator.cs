﻿using FluentValidation;

namespace EventManagementAPI.Application.EventItems.Commands.CreateEvent
{
    public class CreateEventCommandValidator : AbstractValidator<CreateEventCommand>
    {
        public CreateEventCommandValidator()
        {
            RuleFor(v => v.Name)
             .NotEmpty();

            RuleFor(v => v.Description)
             .NotEmpty();

            RuleFor(v => v.StartDate)
             .NotEmpty();

            RuleFor(v => v.EndDate)
            .NotEmpty();
        }        
    }
}
