﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using EventManagementAPI.Application.Common.Exceptions;
using EventManagementAPI.Application.Common.Interfaces;
using EventManagementAPI.Domain.Entities;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EventManagementAPI.Application.EventItems.Queries.GetEvent
{
    public class GetEventQuery : IRequest<EventViewModel>
    {
        public long Id { get; set; }
    }

    public class GetEventQueryHandler : IRequestHandler<GetEventQuery, EventViewModel>
    {
        private readonly IApplicationDbContext _context;
        private readonly IMapper _mapper;

        public GetEventQueryHandler(IApplicationDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<EventViewModel> Handle(GetEventQuery request, CancellationToken cancellationToken)
        {
            var vm = await _context.Event
                .Where(t => t.Id == request.Id)
                .ProjectTo<EventViewModel>(_mapper.ConfigurationProvider)
                .FirstOrDefaultAsync(cancellationToken);

            if (vm == null)
            {
                throw new NotFoundException(nameof(Event), request.Id);
            }

            return vm;
        }
    }
}
