﻿using EventManagementAPI.Application.Common.Mappings;
using EventManagementAPI.Domain.Entities;
using System;

namespace EventManagementAPI.Application.EventItems.Queries.GetEvent
{
    public class EventViewModel: IMapFrom<Event>
    {
        public string Name { get; set; }
        public DateTime StartDate { get; set; }
        public bool OneDayEvent { get; set; }
        public DateTime EndDate { get; set; }
        public string Location { get; set; }
        public string Description { get; set; }

        //TODO add list of Agenda
    }
}
