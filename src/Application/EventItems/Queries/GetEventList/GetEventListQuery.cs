﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using EventManagementAPI.Application.Common.Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EventManagementAPI.Application.EventItems.Queries.GetEventList
{
    public class GetEventListQuery : IRequest<EventListViewModel>
    {
        public class GetEventListQueryHandler : IRequestHandler<GetEventListQuery, EventListViewModel>
        {
            private readonly IApplicationDbContext _context;
            private readonly IMapper _mapper;

            public GetEventListQueryHandler(IApplicationDbContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }
            public async Task<EventListViewModel> Handle(GetEventListQuery request, CancellationToken cancellationToken)
            {
                var items = await _context.Event
                    .ProjectTo<EventDTO>(_mapper.ConfigurationProvider)
                    .ToListAsync(cancellationToken);

                var vm = new EventListViewModel
                {
                    Events = items
                };

                return vm;
            }
        }
    }
}
