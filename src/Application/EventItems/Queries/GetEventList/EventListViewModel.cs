﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EventManagementAPI.Application.EventItems.Queries.GetEventList
{
    public class EventListViewModel
    {
        public IList<EventDTO> Events { get; set; }
    }
}
