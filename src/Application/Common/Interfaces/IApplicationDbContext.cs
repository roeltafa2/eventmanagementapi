﻿using EventManagementAPI.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace EventManagementAPI.Application.Common.Interfaces
{
    public interface IApplicationDbContext
    {
        DbSet<Event> Event { get; set; }
        DbSet<Agenda> Agenda { get; set; }

        Task<int> SaveChangesAsync(CancellationToken cancellationToken);
    }
}
