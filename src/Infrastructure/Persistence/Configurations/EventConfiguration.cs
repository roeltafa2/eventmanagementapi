﻿using EventManagementAPI.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace EventManagementAPI.Infrastructure.Persistence.Configurations
{
    public class EventConfiguration : IEntityTypeConfiguration<Event>
    {
        public void Configure(EntityTypeBuilder<Event> builder)
        {
            builder.HasKey(k => k.Id);

            builder.Property(t => t.Name)
               .HasMaxLength(200)
               .IsRequired();

            builder.Property(t => t.StartDate)
              .IsRequired();

            builder.Property(t => t.EndDate)
              .IsRequired();

            builder.Property(e => e.Description).HasColumnType("ntext");
        }
    }
}
