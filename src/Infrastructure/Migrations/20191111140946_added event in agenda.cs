﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace EventManagementAPI.Infrastructure.Migrations
{
    public partial class addedeventinagenda : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Agenda_Event_EventId",
                table: "Agenda");

            migrationBuilder.AlterColumn<long>(
                name: "EventId",
                table: "Agenda",
                nullable: false,
                oldClrType: typeof(long),
                oldType: "bigint",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Agenda_Event_EventId",
                table: "Agenda",
                column: "EventId",
                principalTable: "Event",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Agenda_Event_EventId",
                table: "Agenda");

            migrationBuilder.AlterColumn<long>(
                name: "EventId",
                table: "Agenda",
                type: "bigint",
                nullable: true,
                oldClrType: typeof(long));

            migrationBuilder.AddForeignKey(
                name: "FK_Agenda_Event_EventId",
                table: "Agenda",
                column: "EventId",
                principalTable: "Event",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
