﻿using EventManagementAPI.Domain.Interfaces;
using System;

namespace EventManagementAPI.Infrastructure.Services
{
    public class DateTimeService : IDateTime
    {
        public DateTime Now => DateTime.Now;
    }
}
