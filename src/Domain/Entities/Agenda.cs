﻿using EventManagementAPI.Domain.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace EventManagementAPI.Domain.Entities
{
    public class Agenda : BaseEntity
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime Time { get; set; }
        public string TimeFrom { get; set; }
        public string TimeTo { get; set; }
        public long EventId { get; set; }
        public Event Event { get; set; }
    }
}
