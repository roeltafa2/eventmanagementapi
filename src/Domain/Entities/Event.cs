﻿using EventManagementAPI.Domain.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace EventManagementAPI.Domain.Entities
{
    public class Event : BaseEntity
    {
        public Event()
        {
            Agendas = new HashSet<Agenda>();
        }
        public string Name { get; set; }
        public DateTime StartDate { get; set; }
        public bool OneDayEvent { get; set; }
        public DateTime EndDate { get; set; }
        public string Location { get; set; }
        public string Description { get; set; }
        public ICollection<Agenda> Agendas { get; set; }
    }
}
