﻿using System;

namespace EventManagementAPI.Domain.Interfaces
{
    public interface IDateTime
    {
        DateTime Now { get; }
    }
}
